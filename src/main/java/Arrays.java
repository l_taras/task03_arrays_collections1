import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Arrays {


    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<Integer>();
        List<Integer> list2 = new ArrayList<Integer>();
        list1.add(2);
        list1.add(5);
        list1.add(5);
        list1.add(5);
        list1.add(5);
        list1.add(5);
        list1.add(6);
        list1.add(6);
        list1.add(6);
        list1.add(0);
        list1.add(7);
        list2.add(0);
        list2.add(2);
        list2.add(6);
        list2.add(9);

//        contains(list1, list2);


//        list1 = uniq(list1, list2);
        list1 = delete2(list1);


        for (Integer integer : list1) {
            System.out.println(integer);

        }
    }

    // TaskA присутні в обох масивах
    public static List<Integer> contains(List<Integer> list1, List<Integer> list2) {
        list1.retainAll(list2);
        return list1;
    }

    // TaskA б) присутні тільки в одному з масивів.
    public static List<Integer> uniq(List<Integer> list1, List<Integer> list2) {
        list1.removeAll(list2);
        return list1;
    }

    //    B. Видалити в масиві всі числа, які повторюються більше двох разів.
    public static List<Integer> delete2(List<Integer> list1) {
        List<Integer> result = new ArrayList<Integer>();
        for (Integer integer : list1) {
            if (Collections.frequency(list1, integer) <= 2) {
                result.add(integer);
            }
        }
        return result;
    }

//    C. Знайти в масиві всі серії однакових елементів, які йдуть підряд, і видалити з них всі
// елементи крім одного.


}

