public class Artifact extends Smth {

    public Artifact(int power) {
        super(power);
    }

    @Override
    public String toString() {
        return "Artifact{} " + super.toString();
    }
}
