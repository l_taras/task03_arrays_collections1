public class Room {
    private int number;
    private boolean IsVisited;
    private Smth smth;

    public Room(int number, boolean isVisited, Smth smth) {
        this.number = number;
        IsVisited = isVisited;
        this.smth = smth;
    }

    @Override
    public String toString() {
        return "Room{" +
                "number=" + number +
                ", IsVisited=" + IsVisited +
                ", smth=" + smth +
                '}';
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isVisited() {
        return IsVisited;
    }

    public void setVisited(boolean visited) {
        IsVisited = visited;
    }

    public Smth getSmth() {
        return smth;
    }

    public void setSmth(Smth smth) {
        this.smth = smth;
    }
}
