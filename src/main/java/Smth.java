public abstract class Smth {
    private int power;

    public Smth(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }


    @Override
    public String toString() {
        return "power=" + power +
                '}';
    }
}
