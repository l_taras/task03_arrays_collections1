import Exeptions.GameOver;
import Exeptions.NotANumberException;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Game {
    private List<Room> rooms;
    private Hero hero;
    private int numberOfRooms;

    private Game(Hero hero, int numberOfRooms) {
        this.rooms = fulfill(numberOfRooms);
        this.hero = hero;
        this.numberOfRooms = numberOfRooms;
    }

    private static int getRandom(int min, int max) {
        int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
        return randomNum;
    }

    public static void main(String[] args) {
        Hero hero = new Hero(25);
        int numberOfRooms = 10;
        Game game = new Game(hero, numberOfRooms);
        game.menu();

    }


    private void menu() {
        int input = 0;
        while (true) {
            do {
                input = consoleInput("\nWhat to do?\n1. Open door \n2. Count death \n3. Help \n");
            } while (input < 1 && input > 3);
            switch (input) {
                case 1: {
                    try {
                        System.out.println("you can open room number");
                        for (Room room : rooms) {
                            if (!room.isVisited()) {
                                System.out.print(room.getNumber() + " ");
                            }
                        }
                        int input2 = 0;
                        do {
                            input2 = consoleInput("Enter number of the room");
                        } while (input2 < 1 || input2 > 10);
                        openDoor(input2);
                    } catch (GameOver gameOver) {
                        System.err.println("Game over");
                        gameOver.printStackTrace();
                        System.exit(0);
                    }
                    break;
                }
                case 2: {
                    count();
                    break;
                }
                case 3: {
                    help();
                    break;
                }
            }
        }
    }

    //3. Порахувати, за скількома дверима героя чекає смерть. Рекурсивно.
    private void count() {
        int death = 0;
        for (Room room : rooms) {
            if ((!room.isVisited()) && (room.getSmth() instanceof Monster)
                    && room.getSmth().getPower() > hero.getPower()) {
                death++;
            }
        }
        System.out.println("Death is waiting for you in " + death + " rooms");
    }

    private void help() {
        List<Room> roomsWithMonster = new ArrayList<>();
        for (Room room : this.rooms) {
            if (!room.isVisited()) {
                roomsWithMonster.add(room);
            }
        }
        List<Room> roomsWithArtifact = new ArrayList<>();
        for (Room room : roomsWithMonster) {
            if (room.getSmth() instanceof Artifact) {
                roomsWithArtifact.add(room);
            }
        }
        roomsWithMonster.removeAll(roomsWithArtifact);
        Collections.sort(roomsWithMonster, (Room a, Room b) -> b.getSmth().getPower() - a.getSmth().getPower());
        System.out.println("Order to open safety: room nubmer:");
        for (Room room : roomsWithArtifact) {
            System.out.print(room.getNumber() + " ");

        }
        for (Room room : roomsWithMonster) {
            if (hero.power >= room.getSmth().getPower())
                System.out.print(room.getNumber() + " ");
        }
    }

    private static List<Room> fulfill(int numberOfRooms) {
        List<Room> listRooms = new ArrayList<>();
        Random random = new Random();
        for (int i = 1; i <= numberOfRooms; i++) {
            if (random.nextBoolean()) {
                Monster monster = new Monster(getRandom(5, 100));
                Room room = new Room(i, false, monster);
                listRooms.add(room);
            } else {
                Artifact artifact = new Artifact(getRandom(10, 80));
                Room room = new Room(i, false, artifact);
                listRooms.add(room);
            }
        }
        return listRooms;
    }

    private void openDoor(int num) throws GameOver {
        num--;
        if (rooms.get(num).isVisited()) {
            System.out.println("This Room is already visited");
            return;
        }
        rooms.get(num).setVisited(true);
        if (rooms.get(num).getSmth() instanceof Monster) {
            if (hero.power >= rooms.get(num).getSmth().getPower()) {

                System.out.println("\n Hero wins \n");
            } else {
                System.err.println("Hero is dead");
                throw new GameOver();
            }
        }
        if (rooms.get(num).getSmth() instanceof Artifact) {
            System.out.println("Increase power +" + rooms.get(num).getSmth().getPower());
            hero.setPower(hero.getPower() + rooms.get(num).getSmth().getPower());
        }
    }

    public static int consoleInput(String text) {
        Scanner sc = new Scanner(System.in);
        int a = 0;
        System.out.println(text);

        try {
            if (!sc.hasNextInt()) {
                throw new NotANumberException();
            } else a = sc.nextInt();
            return a;
        } catch (NotANumberException e) {
            System.err.println("Not a number, Input again");
            consoleInput(text);
        }
        return a;
    }
}






